import yaml
from flask import Flask, render_template, request

app = Flask(__name__, static_url_path='/assets')


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        source = request.form['yaml']
        app.logger.error(source)
    else:
        source = 'hello: world'
    try:
        result = yaml.load(source)
    except Exception as e:
        result = e
    return render_template('index.html', source=source, result=str(result))
